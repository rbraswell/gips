ARCHIVEDIR="" # path to shared gips archive
AGSFTPCREDS="" # ftp credentials for sixs and aod
EARTHDATA_USER="" # Earthdata credentials (see https://earthdata.nasa.gov)
EARTHDATA_PASS="" # ... for MODIS, MERRA, ASTER
USGS_USER="" # USGS credentials (see https://earthexplorer.usgs.gov)
USGS_PASS="" # ... for Landsat
ESA_USER="" # ESA credentials (see https://scihub.copernicus.eu)
ESA_PASS="" # ... for Sentinel2 and Sentinel1
